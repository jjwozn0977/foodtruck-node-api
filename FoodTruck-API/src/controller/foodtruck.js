import mongoose from 'mongoose';
import  { Router } from 'express';
import FoodTruck from '../model/foodtruck';
import Review from '../model/review';
import { authenticate } from '../middleware/authMiddleware';

export default({config, db}) => {
  let api = Router();

  //'v1/foodtruck/add' - v1 is for version 1, if you do a v2 you dont break v1
  //routes->index.js gets us to v1. routes adds on the /foodtruck which brings us to this file and we append the /add
  //authenticate - will make sure the user is logged in before posting a review
  api.post('/add', authenticate, (req, res) => {
    let newFoodTruck = new FoodTruck();
    newFoodTruck.name = req.body.name;
    newFoodTruck.foodtype = req.body.foodtype;
    newFoodTruck.avgCost = req.body.avgCost;
    newFoodTruck.geometry.coordinates.lat = req.body.geometry.coordinates.lat;
    newFoodTruck.geometry.coordinates.long = req.body.geometry.coordinates.long;


    newFoodTruck.save(err => {
      if(err){
        res.send(err);
      }
      res.json({
        message: 'FoodTruck saved successfully'
      });
    });
  });


  //'v1/foodtruck/ --Read'
  api.get('/', (req, res) => {
    FoodTruck.find({}, (err, foodtrucks) => {
      if(err) {
        res.send(err);
      }
      res.json(foodtrucks);
    });
  });

  //'v1/foodtruck/:id --Read 1'
  api.get('/:id',(req, res) => {
    FoodTruck.findById(req.params.id, (err, foodtruck) => {
      if(err){
        res.send(err);
      }
      res.json(foodtruck);
    });
  });

  //'/v1/foodtruck/:id -- Update'
  //'/find the foodtruck first and then update the info -- PUT
  api.put('/:id',authenticate ,(req, res) => {
    FoodTruck.findById(req.params.id, (err, foodtruck) => {
      if(err){
        res.send(err)
      }
      foodtruck.name = req.body.name;
      foodtruck.type = req.body.type;
      foodtruck.avgCost = req.body.avgCost;
      foodtruck.geometry.coordinates.lat = req.body.geometry.coordinates.lat;
      foodtruck.geometry.coordinates.long = req.body.geometry.coordinates.long;
      foodtruck.save(err => {
        if(err){
          res.send(err);
        }
        res.json({
          message: "foodtruck info updated"
        });
      });
    });
  })


  //'v1/foodtruck/:id --DELETE'
  api.delete('/:id',authenticate ,(req, res) => {
      FoodTruck.findById(req.params.id, (err, foodtruck) => {
        if(err){
          res.status(500).send(err);
          return; //haults execusion if it hits here
        }
        if(foodtruck === null) {
          res.status(404).send("FoodTruck not found");
          return
        }
        FoodTruck.remove({
          _id: req.params.id
        },(err, foodtruck) => {
          if(err){
            res.status(500).send(err);
            return; // without the return the application will still try to remove
          }
        Review.remove({
          foodtruck: req.params.id
        }, (err, review) => {
          if(err){
            res.send(err)
          }
          res.json({
            message: "Food Truck and Reviews Successfully deleted!"
          });
        })
        });
      });
    });


  // add review for a speiciic foodtruck id
  // 'v1/foodtruck/reviews/add/:id'
  api.post('/reviews/add/:id',authenticate ,(req,res) => {
    FoodTruck.findById(req.params.id, (err, foodtruck) => {
      if(err){
        res.send(err)
      }
      let newReview = new Review();
      newReview.title = req.body.title;
      newReview.text = req.body.text;
      //this foodtruck._id is the foodtruck from the parameter list in the parethesis above
      newReview.foodtruck = foodtruck._id;
      newReview.save((err, newReview) => {
        if(err){
          res.send(err)
        }
        //reviews is an array so we need to push this review onto the array
        foodtruck.reviews.push(newReview);
        foodtruck.save(err => {
          if(err){
            res.send(err)
          }
          res.json({
            message: 'Food truck review saved!'
          });
        });
      });
    });
  });

  // get reviews fro a specific food truck id
  // 'v1/foodtruck/reviews/:id'
  api.get('/reviews/:id',(req,res) => {
    Review.find({
      foodtruck: req.params.id
    }, (err, reviews) => {
      if(err){
        res.send(err);
      }
      res.json(reviews);
    });
  });

  return api
}
