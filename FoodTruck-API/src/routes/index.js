  import express from 'express';
  import config from '../config';
  import middleware from '../middleware';
  import initializeDb from '../db';
  import foodtruck from '../controller/foodtruck';
  import account from '../controller/account';

  let router = express();

  //connect to the db
  initializeDb(db => {

    //internal middleware if you want
    router.use(middleware({config, db}));

    //api routes v1v(/v1)
    // the restaurant({config, db}) is a controller that controls the restaurant part of api
    router.use('/foodtruck', foodtruck({config, db}));
    router.use('/account', account({config, db}))

  });

  export default router;
